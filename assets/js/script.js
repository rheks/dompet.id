// Hamburger
const hamburger = document.querySelector(".hamburger");

hamburger.addEventListener("click", function() {
    document.querySelector(".navbar").classList.toggle("show");
});